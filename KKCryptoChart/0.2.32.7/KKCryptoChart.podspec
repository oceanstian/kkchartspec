#
# Be sure to run `pod lib lint KKCryptoChart.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KKCryptoChart'
  s.version          = '0.2.32.7'
  s.summary          = 'A Chart Framework for Aspen'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
    TODO: Add long description of the pod here.
  DESC

  s.homepage         = 'https://gitlab.com/oceanstian/kkcryptochart'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Oceanstian' => 'oceanstian@aspendigital.co' }
  s.source           = { :git => 'https://gitlab.com/oceanstian/kkcryptochart.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'KKCryptoChart/Classes/**/*'
  
  s.resources    = "KKCryptoChart/Assets/*.{png,bundle}"

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.pod_target_xcconfig = { 'DEFINES_MODULE'=>'YES' }
  s.frameworks = 'UIKit'
  s.dependency 'KKCharts', '3.6.7.2'
  s.dependency 'SocketRocket'
  s.swift_version = '5.3'
end
